import React from 'react'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import { Box, CircularProgress, Paper, TableSortLabel } from '@material-ui/core'
import { OrderBy } from '../types'
import { timeStampToDateString } from '../utils'

type PresentationalTable = {
  data: any
  toggleOrder: () => void
  orderBy: OrderBy
  showError: boolean
  isLoading: boolean
  loadData: () => void
  isProductTable?: boolean
}

export const PresentationTable: React.FunctionComponent<PresentationalTable> = ({
  toggleOrder,
  orderBy,
  data,
  isLoading,
  showError,
  loadData,
  isProductTable,
}) => (
  <Box my={6}>
    <TableContainer component={Paper} data-testid="table-container">
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>
              <TableSortLabel onClick={toggleOrder} direction={orderBy}>
                Date
              </TableSortLabel>
            </TableCell>
            <TableCell>{isProductTable ? 'Product ID' : 'User ID'}</TableCell>
            <TableCell>Old Value</TableCell>
            <TableCell>New Value</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((data) => (
            <TableRow key={data.id} data-testid="table-row">
              <TableCell component="th" scope="row">
                {timeStampToDateString(data.timestamp)}
              </TableCell>
              <TableCell>{data.id}</TableCell>
              <TableCell>{data.diff[0].oldValue}</TableCell>
              <TableCell>{data.diff[0].newValue}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <Box m={1} display="flex" flexDirection="column" alignItems="center" justifyContent="center">
        {showError && (
          <Typography color="secondary" align="center" data-testid="error-text">
            We had problems fetching your data. Please try again.
          </Typography>
        )}
        <Box height="50px" display="flex" alignItems="center">
          {isLoading ? (
            <CircularProgress data-testid="loading-progress" />
          ) : (
            <Button
              variant="contained"
              color="primary"
              onClick={loadData}
              data-testid="load-data-button"
            >
              {showError ? 'Retry' : 'Load more'}
            </Button>
          )}
        </Box>
      </Box>
    </TableContainer>
  </Box>
)
