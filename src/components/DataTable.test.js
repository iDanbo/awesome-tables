import React from 'react'
import { render } from '@testing-library/react'
import { DataTable } from './DataTable'

describe('<DataTable />', () => {
  it('renders the table container', () => {
    const { getByTestId } = render(<DataTable />)
    expect(getByTestId('table-container')).toBeInTheDocument()
  })
})
