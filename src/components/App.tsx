import React from 'react'
import Container from '@material-ui/core/Container'
import Box from '@material-ui/core/Box'
import { DataTable } from './DataTable'

const App = () => {
  return (
    <Box bgcolor="#d0d0e6" borderRadius="8px">
      <Container className="app">
        <Box display="flex" flexDirection="column" data-testid="app-box" m={2}>
          <DataTable />
          <DataTable isProductTable />
        </Box>
      </Container>
    </Box>
  )
}

export default App
