import React from 'react'
import { render } from '@testing-library/react'
import App from './App'

describe('<App />', () => {
  it('renders the Box', () => {
    const { getByTestId } = render(<App />)
    expect(getByTestId('app-box')).toBeInTheDocument()
  })
})
