import React, { useEffect, useReducer, useState } from 'react'
import api from '../lib/api'
import { reducer } from '../reducer'
import { PresentationTable } from './PresentationTable'

type DataTableProps = {
  isProductTable?: boolean
}

export const DataTable: React.FunctionComponent<DataTableProps> = ({ isProductTable }) => {
  const [isLoading, setIsLoading] = useState(false)
  const [showError, setShowError] = useState(false)
  const [state, dispatch] = useReducer(reducer, { data: [], orderBy: 'desc' })

  const toggleOrder = () => {
    if (state.orderBy === 'asc') {
      dispatch({ type: 'desc' })
    } else {
      dispatch({ type: 'asc' })
    }
  }

  const fetchData = async () => {
    try {
      setIsLoading(true)
      const result = isProductTable ? await api.getProjectsDiff() : await api.getUsersDiff()
      setIsLoading(false)
      dispatch({ type: state.orderBy, data: result.data })
      setShowError(false)
    } catch (e) {
      setShowError(true)
      setIsLoading(false)
    }
  }

  useEffect(() => {
    fetchData()
  }, [])

  return (
    <PresentationTable
      data={state.data}
      isLoading={isLoading}
      showError={showError}
      loadData={fetchData}
      toggleOrder={toggleOrder}
      orderBy={state.orderBy}
      isProductTable={isProductTable}
    />
  )
}
