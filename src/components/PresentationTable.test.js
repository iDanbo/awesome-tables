import React from 'react'
import { render } from '@testing-library/react'
import { DataTable } from './DataTable'
import { PresentationTable } from './PresentationTable'

const data = [
  {
    id: 'e28d290a-a2f2-48c2-9001-ff43884e271b',
    timestamp: new Date('2020/2/14').getTime(),
    diff: [{ field: 'name', oldValue: 'John', newValue: 'Bruce' }],
  },
  {
    id: '8bd0166f-f0c6-48fd-9fcd-a17e76eb1e92',
    timestamp: new Date('2020/2/15').getTime(),
    diff: [{ field: 'name', oldValue: 'Bruce', newValue: 'Nick' }],
  },
  {
    id: '0a75a2b3-be64-4aeb-ba4c-8ddb913791ac',
    timestamp: new Date('2020/2/16').getTime(),
    diff: [{ field: 'name', oldValue: 'Nick', newValue: 'Michel' }],
  },
]

describe('<PresentationTable />', () => {
  const toggleOrder = jest.fn()
  const loadData = jest.fn()

  it('renders the table with 3 rows', () => {
    const { getAllByTestId } = render(
      <PresentationTable
        data={data}
        toggleOrder={toggleOrder}
        loadData={loadData}
        orderBy="asc"
        showError={false}
        isLoading={false}
      />,
    )
    expect(getAllByTestId('table-row')).toHaveLength(3)
  })
  it('renders the table in loading state', () => {
    const isLoading = true
    const { getByTestId } = render(
      <PresentationTable
        data={data}
        toggleOrder={toggleOrder}
        loadData={loadData}
        orderBy="asc"
        showError={false}
        isLoading={isLoading}
      />,
    )
    expect(getByTestId('loading-progress')).toBeInTheDocument()
  })
  it('renders the table in error state', () => {
    const showError = true
    const { getByTestId } = render(
      <PresentationTable
        data={data}
        toggleOrder={toggleOrder}
        loadData={loadData}
        orderBy="asc"
        showError={showError}
        isLoading={false}
      />,
    )
    expect(getByTestId('error-text')).toBeInTheDocument()
  })
})
