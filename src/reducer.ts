export const reducer = (state, action) => {
  let data = state.data
  if (action.data) {
    data = [...state.data, ...action.data]
  }
  const sortedData = [...data].sort((a, b) =>
    action.type === 'desc' ? b.timestamp - a.timestamp : a.timestamp - b.timestamp,
  )
  return { data: sortedData, orderBy: action.type }
}