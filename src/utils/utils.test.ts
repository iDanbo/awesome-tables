import { timeStampToDateString } from "."

describe('utils', () => {
    describe('timeStampToDateString', () => {
        it('should format timestamp to date string', () => {
            expect(timeStampToDateString(1581804000000)).toBe('16/02/2020')
        })
    })
    
})
